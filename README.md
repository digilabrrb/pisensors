# README #

###Some ground rules:###

1. All members to use official email id only. 

2. No intellectual property to be uploaded on this private repository. (VDI by default blocks any such activity anyway)

3. No pirated / cracked content to be used in this repository.

4. Follow licensing practices on this repo.


### What is this repository for? ###

* Repo to maintian demo examples on RPi Sensors
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)



### How do I get set up? ###

*To be updated*

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

Add the activity/idea you are planning to do in digilab log

* Add scripts with standard PEP conventions
* Code review (Optional)
* Describe the steps to run the script

### Who do I talk to? ###

* For Bitbucket Permissions - Rajat/Mallika